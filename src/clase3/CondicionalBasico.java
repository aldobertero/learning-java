package clase3;

import java.util.Scanner;

public class CondicionalBasico {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese un número entero");
        int entero = scanner.nextInt();
        if (entero % 2 == 0) {
            System.out.println("Ingresaste un número par.");
        } else {
            System.out.println("Ingresaste un número impar.");
        }
    }
}
