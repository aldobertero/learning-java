package clase3;

import java.util.Scanner;

public class CondicionalExtendido {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qué nota te sacaste este semestre?");
        double nota = scanner.nextDouble();
        if (nota >= 6.5) {
            System.out.println("Aprobaste con distinción máxima");
        } else if (nota >= 6.0) {
            System.out.println("Aprobaste con distinción");
        } else if (nota >= 4.0) {
            System.out.println("Aprobaste");
        } else if (nota >= 3.9) {
            System.out.println("Vas a examen adicional");
        } else {
            System.out.println("Reprobaste");
        }
    }
}
