package clase5;

public class Arreglos {

    public static void main(String[] args) {
        int[] numeros = new int[10];
        numeros[3] = 40;
        for (int numero : numeros) {
            System.out.println(numero);
        }
    }
}
