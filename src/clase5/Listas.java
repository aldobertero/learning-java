package clase5;

import java.util.ArrayList;
import java.util.List;

public class Listas {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        lista.add("Un nombre cualquiera");
        lista.add("Otro nombre");
        lista.add("Un tercer nombre");
        lista.add("Un cuarto nombre");
        System.out.println("Elementos:");
        for (String elemento : lista) {
            System.out.println(elemento);
        }
        System.out.println("Tamaño de lista: " + lista.size());
        System.out.println("Contiene 'Otro nombre'? " + lista.contains("Otro nombre"));
        lista.remove(0);
        System.out.println("Primer elemento luego de borrar el 0: " + lista.get(0));
        lista.clear();
        System.out.println("Se limpió adecuadamente? " + lista.isEmpty());
    }
}
