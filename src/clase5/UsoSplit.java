package clase5;

public class UsoSplit {

    public static void main(String[] args) {
        String codigoEnClave = "13123-4#Carlos#Gonzalez#22/4/1981#Buenos Aires";
        String[] codigoDescifrado = codigoEnClave.split("#");
        for (String codigo : codigoDescifrado) {
            System.out.println(codigo);
        }
    }
}
