package clase1;

import java.util.Scanner;

public class LeyendoEntradas {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Cual es tu nombre?");
        String nombre = scanner.nextLine();
        System.out.println("Hola " + nombre + ", cual es tu edad?");
        int edad = scanner.nextInt();
        System.out.println("Ok, " + nombre + ", tienes " + edad + " años");
    }
}
