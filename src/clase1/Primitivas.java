package clase1;

public class Primitivas {

    public static void main(String[] args) {
        boolean operadorLogico = false;
        short enteroCorto = 12311;
        int entero = 312312312;
        float enteroUnPocoMasLargo = 1231231231;
        long enteroLargo = 323132131;
        double numeroConDecimales= 62323423123132312312312311231312123121312331312312312311634343432312323312312321.012313;
        char caracter = 'a';
        byte bite = 0x00f;

        System.out.println(operadorLogico);
        System.out.println(enteroCorto);
        System.out.println(entero);
        System.out.println(enteroUnPocoMasLargo);
        System.out.println(enteroLargo);
        System.out.println(numeroConDecimales);
        System.out.println(caracter);
        System.out.println(bite);

    }
}
