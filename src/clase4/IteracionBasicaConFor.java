package clase4;

import java.util.Scanner;

public class IteracionBasicaConFor {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("¿Hasta qué numero quiere que contemos?");
        int number = scanner.nextInt();
        if (number < 0) {
            System.out.println("Se requiere un número positivo");
        } else {
            for (int contador = 0; contador <= number; ++contador) {
                System.out.println("Ahora toca el " + contador);
            }
        }
    }
}
