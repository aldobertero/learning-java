package clase4;

import java.util.Scanner;

public class IteracionBasica {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("¿Hasta qué numero quiere que contemos?");
        int number = scanner.nextInt();
        if (number < 0) {
            System.out.println("Se requiere un número positivo");
        } else {
            int contador = 0;
            while (contador <= number) {
                System.out.println("Ahora toca el " + contador);
                contador = contador + 1;
            }
        }
    }
}
