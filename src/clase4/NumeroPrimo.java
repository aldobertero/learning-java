package clase4;

import java.util.Scanner;

public class NumeroPrimo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese un número entero positivo");
        int numero = scanner.nextInt();
        for (int i = 2; i <= numero; ++i) {
            if (esNumeroPrimo(i)) {
                System.out.println(i + " es número primo");
            }
        }
    }

    public static boolean esNumeroPrimo(int numero) {
        int mitadNumero = numero / 2 + 1;
        if (numero == 2) {
            return true;
        }
        for (int i = 2; i <= mitadNumero; ++i) {
            if (numero % i == 0) {
                return false;
            }
        }
        return true;
    }
}
