package clase2;

import java.util.Scanner;

public class OperadoresLogicos {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese un número: ");
        int primerNumero = scanner.nextInt();
        System.out.println("Ingrese otro número: ");
        int segundoNumero = scanner.nextInt();
        boolean mayorQue = primerNumero > segundoNumero;
        boolean mayorOIgualQue = primerNumero >= segundoNumero;
        boolean menorQue = primerNumero < segundoNumero;
        boolean menorOIgualQue = primerNumero <= segundoNumero;
        boolean igualQue = primerNumero == segundoNumero;
        boolean distintoQue = primerNumero != segundoNumero;
        System.out.println(primerNumero + " mayor que " + segundoNumero + " ? " + mayorQue);
        System.out.println(primerNumero + " mayor o igual que " + segundoNumero + " ? " + mayorOIgualQue);
        System.out.println(primerNumero + " menor que " + segundoNumero + " ? " + menorQue);
        System.out.println(primerNumero + " menor o igual que " + segundoNumero + " ? " + menorOIgualQue);
        System.out.println(primerNumero + " igual que " + segundoNumero + " ? " + igualQue);
        System.out.println(primerNumero + " distinto que " + segundoNumero + " ? " + distintoQue);
    }
}
