package clase2;

import java.util.Arrays;

public class OperadoresString {
    public static void main(String[] args) {
        String concatenar = "abc".concat("def");
        int indiceEncontrado = "abcdefgh".indexOf("de");
        int indiceNoEncontrado = "abcdefgh".indexOf("ja");
        String substring = "abcdefgh".substring(4);
        String substringConFin = "abcdefgh".substring(3, 5);
        int largo = "abcdefgh".length();
        String[] split = "este auto es mio y no tuyo lerolero".split(" ");
        String replaceAll = "la mar estaba serena".replaceAll("a", "e");
        boolean startsWithOk = "abcdefghijk".startsWith("abc");
        boolean startsWithNotOk = "abcdefghijk".startsWith("de");
        boolean endsWithOk = "abcdefghijk".endsWith("ijk");
        boolean endsWithNotOk = "abcdefghijk".startsWith("de");
        boolean vacio = "".isEmpty();
        boolean noVacio = "asdf".isEmpty();
        boolean espacioNoEsVacio = " ".isEmpty();
        String trimeado = "    hola que tal?    ".trim();
        System.out.println("concat: " + concatenar);
        System.out.println("indexOf: " + indiceEncontrado);
        System.out.println("indexOf no encontrado: " + indiceNoEncontrado);
        System.out.println("substring: " + substring);
        System.out.println("substring con fin: " + substringConFin);
        System.out.println("length: " + largo);
        System.out.println("split: " + Arrays.toString(split));
        System.out.println("replaceAll: " + replaceAll);
        System.out.println("startsWith ok: " + startsWithOk);
        System.out.println("startsWith no ok: " + startsWithNotOk);
        System.out.println("endsWith ok: " + endsWithOk);
        System.out.println("endsWith no ok: " + endsWithNotOk);
        System.out.println("trim: " + trimeado);
        System.out.println("isEmpty: " + vacio);
        System.out.println("isEmpty falso: " + noVacio);
        System.out.println("isEmpty espacio: " + espacioNoEsVacio);
    }
}
