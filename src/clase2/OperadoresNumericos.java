package clase2;

import java.util.Scanner;

public class OperadoresNumericos {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese un número: ");
        int primerNumero = scanner.nextInt();
        System.out.println("Ingrese otro número: ");
        int segundoNumero = scanner.nextInt();
        int suma = primerNumero + segundoNumero;
        int resta = primerNumero - segundoNumero;
        int multiplicacion = primerNumero * segundoNumero;
        int division = primerNumero / segundoNumero;
        int resto = primerNumero % segundoNumero;
        System.out.println(primerNumero + " + " + segundoNumero + " = " + suma);
        System.out.println(primerNumero + " - " + segundoNumero + " = " + resta);
        System.out.println(primerNumero + " * " + segundoNumero + " = " + multiplicacion);
        System.out.println(primerNumero + " / " + segundoNumero + " = " + division);
        System.out.println(primerNumero + " % " + segundoNumero + " = " + resto);
    }
}
